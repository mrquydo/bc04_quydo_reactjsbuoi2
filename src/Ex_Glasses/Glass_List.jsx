import React, { Component } from 'react'

export default class Glass_List extends Component {
    render() {
        return (
            <button
                onClick={() => {
                    this.props.handleShowGlassDetail(this.props.detail.id)
                }}
                className="card col-2">
                <img className="card-img-top img-fluid" src={this.props.detail.url} alt="Card image cap" />
            </button>
        )
    }
}
